/* This file show algorith of finding
   current someone's location and nearest ATMs locations,
   Authored by Mikidadi Kipolelo
 */


// depencencies declarations
import java.util.HashMap;   

import java.util.Map;        

import java.util.PriorityQueue;


public class PrioRQueueExample {

public static void main(String[] args){

// For putting  ATM's comperative distances relative to current location
PriorityQueue<Double> priority_queue = new PriorityQueue<Double>((x,y)-> {Double m = y-x;return m.intValue(); });

PrioRQueueExample pqe = new PrioRQueueExample();

// Setting curent location and Number of nearest ATMs to return
double current_location = 0.00;

int number_of_ATMs = 4;

// For Key-value assigning for a Map
Map<String,Double> nallATMLocs = new HashMap<String,Double>();

//Addding ATM names and their distance co-ordinates
nallATMLocs.put("ATM1",96.0);

nallATMLocs.put("ATM2",45.0);

nallATMLocs.put("ATM3",17.0);

nallATMLocs.put("ATM4",99.0);

nallATMLocs.put("ATM5",24.0);

nallATMLocs.put("ATM6",76.0);

nallATMLocs.put("ATM7",55.0);

nallATMLocs.put("ATM8",1.00);

nallATMLocs.put("ATM9",3.00);

nallATMLocs.put("ATM10",85.00);

//check if priority queue has a elements if not add
nallATMLocs.forEach((atm,distance) ->{if(priority_queue.size() < number_of_ATMs){

priority_queue.add(pqe.getLocation(current_location,distance));}

else{

if(        
priority_queue.peek() > pqe.getLocation(current_location,distance)){

priority_queue.poll();

priority_queue.add(pqe.getLocation(current_location,distance));

}

}

});

// Printing current location and nearest ATM's Locations remains in a queue
System.out.println("Current Location: " + current_location);
priority_queue.forEach(atmLocation -> System.out.println(atmLocation));

}

// For calculating distance difference btn current location & atm
private double getLocation(double current,double atm){

return atm - current;

}

}